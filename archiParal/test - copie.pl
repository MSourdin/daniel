    use strict;
    use warnings;

    for(my $k = 0; $k < 5; $k++)
    {
     
	    my $nom_de_fichier;
	    my @values;

    	if($k == 0)
    	{
    		$nom_de_fichier = 'Prog31.c';
    		@values = (64, 512, 1000, 1024, 2048);
    	}
    	if($k == 1)
    	{
    		$nom_de_fichier = 'Prog32.c';
    		@values = (64, 100, 512, 1024);
    	}
    	if($k == 2)
    	{
    		$nom_de_fichier = 'Prog33.c';
    		@values = (16, 64, 100);
    	}
    	if($k == 3)
    	{
    		$nom_de_fichier = 'Prog33T.c';
    		@values = (16, 64, 100);
    	}
    	if($k == 4)
    	{
    		$nom_de_fichier = 'Prog33ikj.c';
    		@values = (16, 64, 100);
    	}

	    my $size = @values;
	    my $dir = substr $nom_de_fichier, 0, -2;
    	my $delete = system("rm -rf $dir");

	    mkdir $dir;
	    chdir($dir) or die "$!";

	    printf("== $dir ==\n");
	    
	    for(my $i = 0; $i < $size; $i++)
	    {
		    my $donnees = lire_fichier("../$nom_de_fichier");

		    $donnees =~ s/#define N 64/#define N $values[$i]/g;
		    ecrire_fichier("$values[$i].c", $donnees);

		    system("gcc -o exe$values[$i] $values[$i].c 2> /dev/null");
		    system("./exe$values[$i]");
		    printf("Exécution de $dir pour N = $values[$i]\n");
	    }

	    chdir("..") or die "$!";
	    printf("\n\n");
    }    
    
    exit;
     
    sub lire_fichier
    {
	    my ($nom_de_fichier) = @_;
	     
	    open my $entree, '<:encoding(UTF-8)', $nom_de_fichier or die "Impossible d'ouvrir '$nom_de_fichier' en lecture : $!";
	    local $/ = undef;
	    my $tout = <$entree>;
	    close $entree;
	     
	    return $tout;
    }
     
    sub ecrire_fichier 
    {
	    my ($nom_de_fichier, $contenu) = @_;
	     
	    open my $sortie, '>:encoding(UTF-8)', $nom_de_fichier or die "Impossible d'ouvrir '$nom_de_fichier' en écriture : $!";
	    print $sortie $contenu;
	    close $sortie;
	     
	    return;
	}