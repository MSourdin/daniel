\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Produit-Scalaire}{4}
\contentsline {subsection}{\numberline {2.1}N=64}{4}
\contentsline {subsection}{\numberline {2.2}N=512}{5}
\contentsline {section}{\numberline {3}Matrice-Vecteur}{5}
\contentsline {subsection}{\numberline {3.1}Avant-go\IeC {\^u}t}{5}
\contentsline {subsection}{\numberline {3.2}N=64}{6}
\contentsline {subsection}{\numberline {3.3}N=100}{6}
\contentsline {subsection}{\numberline {3.4} N supeq 512 }{7}
\contentsline {section}{\numberline {4}IJK}{7}
\contentsline {subsection}{\numberline {4.1}Avant-go\IeC {\^u}t}{7}
\contentsline {subsection}{\numberline {4.2}N = 16}{8}
\contentsline {subsection}{\numberline {4.3}N = 64}{8}
\contentsline {section}{\numberline {5}IKJ}{8}
\contentsline {subsection}{\numberline {5.1}Avant-go\IeC {\^u}t}{8}
\contentsline {subsection}{\numberline {5.2}N = 16}{9}
\contentsline {subsection}{\numberline {5.3}N = 64}{9}
\contentsline {subsection}{\numberline {5.4}N = 100}{9}
\contentsline {section}{\numberline {6}IJKT}{10}
\contentsline {subsection}{\numberline {6.1}Avant-go\IeC {\^u}t}{10}
\contentsline {subsection}{\numberline {6.2}N = 16}{10}
\contentsline {subsection}{\numberline {6.3}N supeq 64}{11}
\contentsline {section}{\numberline {7}Conclusion}{11}
\contentsline {section}{\numberline {8}Annexes - Les r\IeC {\'e}sultats}{12}
